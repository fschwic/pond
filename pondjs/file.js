var fs = require('fs');
var crypto = require('crypto');

exports.makeDirsForFile = function(file) {
  var dir = file.substring(0, file.lastIndexOf('/'));
  console.log("directories " + dir);
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }
}

exports.storeStream = function(data, file, info, cb) {
  this.makeDirsForFile(file);
  var cT = this.createThumb;
  var cC = this.createChecksum;
  var cI = this.createInfo;

  var writeable = fs.createWriteStream(file);
  writeable.on('finish', function() {



    var thumbFileName = cT(file);
    var checksumFileName = cC(file);
    info.thumbnail = thumbFileName;
    info.checksum = checksumFileName;
    cI(file, info);



    cb();
  });
  writeable.end(data);

}

exports.createThumb = function(file) {
  return file.replace('/master/', '/.derivates/') + ".thmb";
}

exports.createChecksum = function(file) {
  var hash_algorithm = "sha256";
  var checksumFileName = file.replace('/master/', '/.checksums/') + "." + hash_algorithm;

  function computeHash(filepath, algorithm, callback) {
    var hash = crypto.createHash(algorithm)
    var rs = fs.createReadStream(filepath)

    rs.on('open', function() {})

    rs.on('error', function(err) {
      throw err
    })

    rs.on('data', function(chunk) {
      hash.update(chunk)
    })

    rs.on('end', function() {
      hash = hash.digest('hex')
      return callback(null, hash)
    })
  }

  computeHash(file, hash_algorithm, function(err, hash) {
    if (err) {
      throw err
    }
    module.exports.makeDirsForFile(checksumFileName);
    var writeable = fs.createWriteStream(checksumFileName);
    writeable.on('finish', function() {
      console.log("checksum written " + checksumFileName);
    });
    writeable.end(hash);
  });

  return checksumFileName;
}

exports.createInfo = function(file, info) {
  var infoFileName = file.replace('/master/', '/info/') + ".json";
  /* TODO overwritable?
  if (fs.existsSync(infoFileName)) {
    // don't overwrite
    throw "File already exists. " + infoFileName;
  }
  */

  function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0,
        v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

  function uuidv4_crypto() {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
      (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
  }

  function create_UUID() {
    var dt = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = (dt + Math.random() * 16) % 16 | 0;
      dt = Math.floor(dt / 16);
      return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
  }
  var uuid = create_UUID();
  console.log(uuid);

  info.guid = uuid;
  info.title = file.substring(file.lastIndexOf('/') + 1);
  if (!info.classification) {
    info.classification = "private";
  }
  var d = new Date();
  info.pubDate = d.toISOString();
  //"url": "pond://local/master/fschwic/Planeten/Mars.jpg",


  module.exports.makeDirsForFile(infoFileName);
  var writeable = fs.createWriteStream(infoFileName);
  writeable.on('finish', function() {
    console.log("info written " + infoFileName);
  });
  writeable.end(JSON.stringify(info, null, 2));
}
