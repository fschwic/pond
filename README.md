# The pond

### is able to
- store files send by HTTP PUT
- capable to keep info (JSON), derivates and checksums
- auth users by user JSON from file storage
- public and private (all authenticated users) files

### Run

You need an installation von node.js to run the pond.

Clone the pond from https://bitbucket.org/fschwic/pond.git
or download from https://bitbucket.org/fschwic/pond/downloads/

Change into the directory where `server.js` is located.

Install the necessary node modules by calling
```
npm install
```
Run the pond by
```
node server.js
```

By default the pond is accessible at http://localhost:8081

Create your own user by creating a file `{username}.json` in `pond/local/users/`.
The file should contain an ID, the credentials and a display name. For now,
password is in plain text.
```
{
  "id": 1,
  "username": "fschwic",
  "password": "secret",
  "displayName": "Frank Schwichtenberg",
  "emails": [{
    "value": ""
  }]
}
```

### HTTP interface

#### GET master files
```
http://localhost:8081/pond/fschwic/Planeten/Mars.jpg
```

#### PUT master files
```bash
curl -X PUT 'http://localhost:8081/pond/master/fschwic/Planeten/Mars.jpg' --user "fschwic:secret" -H "Content-Type: image/png" --data-binary @Pictures/Mars.jpg
```

#### GET listings
```
http://localhost:8081/pond/info/*
```


### more to come
