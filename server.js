var fs = require('fs');
var express = require('express');
var handlebars = require('express-handlebars');
var bodyParser = require('body-parser');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var HttpBasicStrategy = require('passport-http').BasicStrategy;
var connectEnsureLogin = require('connect-ensure-login');
var POND = require('./pondjs');

/*
 * Config
 */
var port = process.env.PORT || 8081;
var host = process.env.HOST || "0.0.0.0";
var accessHostPort;

// Configure the local strategy for use by Passport.
//
// The local strategy require a `verify` function which receives the credentials
// (`username` and `password`) submitted by the user.  The function must verify
// that the password is correct and then invoke `cb` with a user object, which
// will be set at `req.user` in route handlers after authentication.
passport.use(new LocalStrategy(verify));
passport.use(new HttpBasicStrategy(verify));

function verify(username, password, done) {
  console.log("verify");
  POND.users.findByUsername(username, function(err, user) {
    console.log("findByUsername callback called");
    if (err) {
      return done(err);
    }
    if (!user) {
      return done(null, false);
    }
    // TODO save encrypted pw
    if (user.password != password) {
      return done(null, false);
    }
    return done(null, user);
  });
}

// Configure Passport authenticated session persistence.
//
// In order to restore authentication state across HTTP requests, Passport needs
// to serialize users into and deserialize users out of the session.  The
// typical implementation of this is as simple as supplying the user ID when
// serializing, and querying the user record by ID from the database when
// deserializing.
passport.serializeUser(function(user, serializationCallback) {
  console.log("serializeUser");
  serializationCallback(null, user.username);
});

passport.deserializeUser(function(username, deserializationCallback) {
  console.log("called deserializeUser: " + username);
  POND.users.findByUsername(username, function(err, user) {
    if (err) {
      return deserializationCallback(err);
    }
    deserializationCallback(null, user);
  });
});

function sendCorsHeaders(req, res, next) {
  var origin = '*';
  if (req.headers.origin) {
    origin = req.headers.origin;
  }
  res.header("Access-Control-Allow-Origin", origin);
  res.header("Access-Control-Allow-Methods", "GET,POST");
  res.header("Access-Control-Allow-Credentials", "true");

  next();
};




// Create a new Express application.
var app = express();
app.use(express.static('static'));
// Use application-level middleware for common functionality, including
// logging, parsing, and session handling.
app.use(require('morgan')('combined'));
app.use(require('express-session')({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: false
}));
// Body parser to be used in routes
// mine app.use(bodyParser.raw({ limit: fileMaxSizeMB + 'mb' })); // bodyparser don't restricts multipart
var bodyUrlEncodedParser = bodyParser.urlencoded({
  extended: true
});
var bodyRawParser = bodyParser.raw({
  type: '*/*',
  limit: '400mb'
});
var bodyJsonParser = bodyParser.json();
// Initialize Passport and restore authentication state, if any, from the
// session.
app.use(passport.initialize());
app.use(passport.session());

/* templating */
var hbs = handlebars.create({
  defaultLayout: 'main'
});
app.engine('handlebars', hbs.engine);
app.set('view engine', 'handlebars');

/*
 * CORS
 */
app.all('*', sendCorsHeaders);


/*
 * Routes
 */
app.get('/',
  function(req, res) {
    console.log(req.user);
    res.render('home', {
      user: req.user
    });
  });

app.get('/login',
  function(req, res) {
    res.render('login', {
      url: req.query.url
    });
  });

app.post('/login', bodyUrlEncodedParser,
  passport.authenticate('local', {
    successReturnToOrRedirect: '/',
    failureRedirect: '/login',
    session: true
  }),
  function(req, res) {
    console.log("from post login function");
    if (req.query.url) {
      res.redirect(req.query.url);
    } else {
      res.redirect('/');
    }
  });

app.get('/logout',
  function(req, res) {
    req.logout();
    res.redirect('/');
  });

app.get('/profile',
  connectEnsureLogin.ensureLoggedIn(),
  function(req, res) {
    res.render('profile', {
      user: req.user
    });
  });

app.get('/private',
  passport.authenticate('basic', {
    session: false
  }),
  function(req, res) {
    res.json(req.user);
  });




/*
 * THE pond
 */

// listings
app.get('/pond/' + 'info/*',
  function(req, res, next) {
    var dir = "./pond/local/" + decodeURI(req.path.substr("/pond/".length));
    if (dir[dir.length - 1] != '/') {
      dir = dir + '/';
    }
    console.log("get listing " + req.path);

    var directoryInfo = {};
    if(fs.existsSync(dir + ".directory")){
      directoryInfo = JSON.parse(fs.readFileSync(dir + ".directory", 'utf8'));
    }

    var listing = {
      title: directoryInfo.title,
      description: directoryInfo.description,
      cover: directoryInfo.cover,
      directories: [],
      files: []
    };

    fs.readdir(dir, {
      withFileTypes: true
    }, function(err, files) {
      //handling error
      if (err) {
        return console.log('Unable to scan directory: ' + err);
      }
      //listing all files using forEach
      files.forEach(function(file) {
        if (file.isDirectory()) {
          listing.directories.push(file.name);
        } else if (file.name[0] != '.') {
          listing.files.push(JSON.parse(fs.readFileSync(dir + file.name, 'utf8')));
        }
      });

      res.json(listing);
    });
  }
);

// Access master
app.get('/pond/' + '*',
  function(req, res, next) {
    console.log("GET " + req.path + ", user: " + req.user);
    var pondLocator = decodeURI(req.path.substr("/pond/".length));
    var infoFile = "/local/info/" + pondLocator;
    var masterFile = "/local/master/" + pondLocator;
    var thumbnailFile = "/local/.derivates/" + pondLocator;

    console.log("pond:/" + infoFile);
    console.log("pond:/" + masterFile);
    console.log("pond:/" + thumbnailFile);

    // check if public
    var info = JSON.parse(fs.readFileSync("pond" + infoFile + '.json', 'utf8'));
    if (info.classification != 'public' && !req.user) {
      res.redirect('/login/?url=' + req.path);
      next();
    } else {

      res.sendFile(masterFile, {
        root: "./pond",
        headers: {
          'Content-disposition': 'filename=' + info.title.replace(' ', '_')
        }
      }, function(err) {
        if (err) {
          console.log("Error Status: " + err.status);
          console.log(err);
          res.status(err.status);
          if (err.status == 404) {
            res.render('not-found');
          } else {
            res.end();
          }
          next();
        } else {
          console.log('Sent:', masterFile);
        }
      });

    }
  });

// put files in the pond
app.put('/pond/master/' + '*', passport.authenticate('basic', {
  session: false
}), bodyRawParser, function(req, res, next) {
  console.log("PUT " + req.path);
  console.log("User " + req.user.username);
  var pondLocator = decodeURI(req.path.substr("/pond/".length));
  // not catched by multer, i.e. not multipart/form-data
  //console.log(req.headers);
  var mimeType = req.headers['content-type'];

  var file = "./pond/local/" + pondLocator;
  console.log("file: " + file + ", mime-type: " + mimeType);
  var locationHost = accessHostPort && accessHostPort.length > 0 ? accessHostPort : req.headers.host;
  var location = req.protocol + "://" + locationHost + "/pond/" + pondLocator.substr("master/".length);

  var info = {
    locator: pondLocator,
    //title: "",
    //description: "",
    //classification: "",
    type: mimeType,
    length: req.headers['content-length']
  };
  /*
  {
    "locator": "master/fschwic/Planeten/Mars.jpg",
    "secret": "",
    "title": "Mars",
    "description": "Der rote Planet",
    "classification": "private",
    "permaLink": "<probably to be set on delivery>",
    "guid": "",
    "pubDate": "Thu,  4 Apr 2019 11:31:32 +0200",
    "url": "pond://local/master/fschwic/Planeten/Mars.jpg",
    "type": "image/jpeg",
    "length": "2129920",
    "source": "<origin context>",
    "thumbnail": ".derivates/fschwic/Planeten/Mars.jpg"
  }
  */
  // write
  POND.file.storeStream(req.body, file, info, function() {
    res.set("Location", location);
    res.status(204).end();
  });
});

app.put('/pond/info' + '*', bodyJsonParser, function(req, res, next) {
  console.log("PUT " + req.path);
  console.log(req.body);
});

/*
 * to actually serv
 */
var server = app.listen(port, host, function() {

  var host = server.address().address
  var port = server.address().port

  console.log('pond is listening at http://%s:%s', host, port)

})
